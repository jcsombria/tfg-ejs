# Colección de Laboratorios Virtuales y Remotos en Easy Javascript Simulations (EJS) 

Introducción
------------
Este repositorio contiene una colección de laboratorios virtuales y remotos enfocados a la docencia en Ciencias e Ingeniería, y
en particular en el modelado, simulación y control de sistemas dinámicos. Todas las aplicaciones han sido desarrolladas en 
Easy Javascript Simulations (EJsS), y se ofrecen de forma libre y gratuita, siempre que sea para un uso académico y no con fines
lucrativos, con la intención de que puedan servir ya sea para uso directo o como fuente de inspiración o ayuda para otros desarrollos. 

Algunas de las aplicaciones interactivas son contribuciones realizadas por alumnos de los grado de Físicas e Ingeniería Electrónica
de Comunicaciones de la Universidad Complutense de Madrid (UCM). Estas simulaciones se ofrecen, con el consentimiento de sus
autores, también de forma libre y gratuita.

Contribuciones
--------------

- Péndulo Invertido -> Pablo Vives 
- Circuitos -> Amalia Pintado 
- Mono-rotor -> Angel del Pino
- Scorbot ER-V Plus -> Alex González 
- Brazo robótico -> Eva María Trapero
